Version beta 3.22 (on the way)

See SQL upgrade directory to update your database from v3.2145

- upgrade bootstrap 4.5.2
- update composer libraries
- new ckeditor version
- new elfinder version
- remove shop rootdirectory
- fix bug on typo Login be LogIn
- fix bug remove products on reviews
- fix and more strict compatibilities and php > 7.4.3
- fix some little bugs : products stock, dimension
- fix email function on string management
- some language translation fix
- Class and function refactoring to minimum php 7.4.3
- add full link for image
- add stripe payment
- Remove Paypal (push on github)
- fix  some offset error
- add wiki information on Github
- update SEO
- add new seo system based on page and header tag
- add sitemap header tag
- Code refactoring
- and more
